package com.wallington.practica1;

import org.junit.Test;

import static org.junit.Assert.*;

public class Equacio2nGrauTest {

    @Test (expected = IllegalArgumentException.class)
    public void constructorTrowsException(){
        new Equacio2nGrau(2, 2, 2);
        new Equacio2nGrau(0, 1, 2);
    }

    @Test
    public void resultatSolucio() {
        Equacio2nGrau equacio2nGrau1 = new Equacio2nGrau(1, 2, 5);
        Equacio2nGrau equacio2nGrau2 = new Equacio2nGrau(1, 4, 1);
        Equacio2nGrau equacio2nGrau3 = new Equacio2nGrau(1, 0, 0);
        assertTrue(equacio2nGrau1.imprimeixSolucio(), equacio2nGrau1.imprimeixSolucio().contains("no te"));
        assertTrue(equacio2nGrau2.imprimeixSolucio(), equacio2nGrau2.imprimeixSolucio().contains("te per solucions"));
        assertTrue(equacio2nGrau2.imprimeixSolucio(), equacio2nGrau3.imprimeixSolucio().contains("te nomes una"));
    }

}
