package com.wallington.practica1;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editText1 = (EditText) findViewById(R.id.editText);
        final EditText editText2 = (EditText) findViewById(R.id.editText2);
        final EditText editText3 = (EditText) findViewById(R.id.editText3);
        final TextView textResultat = (TextView) findViewById(R.id.textView10);

        Button obtenirResultats = (Button) findViewById(R.id.button);

        obtenirResultats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double valueA = Double.parseDouble(String.valueOf(editText1.getText()));
                    double valueB = Double.parseDouble(String.valueOf(editText2.getText()));
                    double valueC = Double.parseDouble(String.valueOf(editText3.getText()));
                    Equacio2nGrau equacio2nGrau = new Equacio2nGrau(valueA, valueB, valueC);
                    textResultat.setText(equacio2nGrau.imprimeixSolucio());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        editText1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editText1.getText().equals("0"))
                    editText1.setText("");

            }
        });

    }

    public void obtenirResultats(View view) {

    }
}
