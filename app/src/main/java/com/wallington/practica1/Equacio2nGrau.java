package com.wallington.practica1;

public class Equacio2nGrau {

    private double a, b, c;
    private double[] solucio = new double[2];

    public Equacio2nGrau(double a, double b, double c) {
        if (a == 0) {
            throw new IllegalArgumentException("'a' no puede ser 0!");
        } else {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }

    public String imprimeixSolucio() {
        String resposta = null;
        double discriminant = Math.pow(b, 2) - 4 * a * c;
        solucio[0] = (-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / 2 * a;
        solucio[1] = (-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / 2 * a;

        if (discriminant < 0) {
            resposta = "L'equacio no te solucions reals.";
        } else if (discriminant > 0) {
            resposta = "L'equacio te per solucions: \nx1 = " + solucio[0] + " \nx2 = " + solucio[1];
        } else {
            resposta = "L'equacio te nomes una solucio: \nx1 = " + solucio[0];
        }
        return resposta;
    }
}
