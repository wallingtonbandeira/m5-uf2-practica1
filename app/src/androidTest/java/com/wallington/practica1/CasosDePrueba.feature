Feature: Test Main Activity

  Background: El usuario esta en la Main Activity

  # Cada scenario es un caso de prueba
  Scenario: Recibir doble resultado
    Given El usuario introduce los parametros a = 1, b = 4, c = 1
    And Pulsa el boton para obtener el resultado
    Then El text layout muestra dos posibles resultados.

  Scenario: Recibir resultado unico
    Given El usuario introduce los parametros a = 1, b = 0, c = 0
    And Pulsa el boton para obtener el resultado
    Then El text layout muestra un unico resultado

  Scenario: No recibir resultado
    Given El usuario introduce los parametros a = 1, b = 2, c = 5
    And Pulsa el boton para obtener el resultado
    Then El text layout dice no haber resultados

  Scenario: No introducir texto
    Given El usuario no introduce ningun parametro
    And Pulsa el boton para obtener el resultado
    Then El text layout no muestra ningun resultado.