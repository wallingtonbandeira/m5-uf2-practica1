package com.wallington.practica1;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.StringContains.containsString;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    /**
     * Clase de test de la Main Activity
     * Los editText solo permiten caracteres numericos
     */

    @Rule
    public ActivityTestRule<MainActivity> mainActivity = new ActivityTestRule<>(
            MainActivity.class, true, true);

    @Test
    public void testUniqueResult() {
        onView(withId(R.id.editText)).perform(typeText("1"));
        onView(withId(R.id.editText2)).perform(typeText("0"));
        onView(withId(R.id.editText3)).perform(typeText("0"));
        onView(withId(R.id.button)).perform(click());

        onView(withId(R.id.textView10))
                .check(matches(withText(containsString("te nomes una"))));
    }

    @Test
    public void testDoubleResult() {
        onView(withId(R.id.editText)).perform(typeText("1"));
        onView(withId(R.id.editText2)).perform(typeText("4"));
        onView(withId(R.id.editText3)).perform(typeText("1"));
        onView(withId(R.id.button)).perform(click());

        onView(withId(R.id.textView10))
                .check(matches(withText(containsString("te per solucions"))));
    }

    @Test
    public void testNoResult() {
        onView(withId(R.id.editText)).perform(typeText("1"));
        onView(withId(R.id.editText2)).perform(typeText("2"));
        onView(withId(R.id.editText3)).perform(typeText("5"));
        onView(withId(R.id.button)).perform(click());

        onView(withId(R.id.textView10))
                .check(matches(withText(containsString("no te"))));
    }

    @Test
    public void testWithoutInsertData() {
        //prueba si el usuario solo ha clicado en el boton sin insertar datos
        onView(withId(R.id.textView10))
                .check(matches(withText("")));
    }

    /**
     * no hace falta añadir pruebas en el caso de  que el usuario inserte texto,
     * los 'editText' puestos solo aceptan numeros
     */

}